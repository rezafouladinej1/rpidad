package com.example.event.category.persistence;

import com.example.event.base.DatabaseConfig;
import com.example.event.event.persistence.Event;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "categories",schema = DatabaseConfig.DB)
public class Category {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Integer id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "category",fetch = FetchType.LAZY)
    private List<Event> events;
}
