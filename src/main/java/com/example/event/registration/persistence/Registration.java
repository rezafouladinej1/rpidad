package com.example.event.registration.persistence;

import com.example.event.base.DatabaseConfig;
import com.example.event.event.persistence.Event;
import com.example.event.user.persistence.User;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "registration",schema = DatabaseConfig.DB)
public class Registration {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Integer id;

    @Column(name = "count")
    private Integer count;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event id",referencedColumnName = "id")
    private Event event;

}

