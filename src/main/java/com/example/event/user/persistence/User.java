package com.example.event.user.persistence;

import com.example.event.base.DatabaseConfig;
import com.example.event.event.persistence.Event;
import com.example.event.registration.persistence.Registration;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "users",schema = DatabaseConfig.DB)
public class User {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Integer id;

    @Column(name = "username",unique = true)
    private String username;

    @Column(name = "phone",unique = true)
    private String phone;

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
    private List<Registration> registrations;



}


