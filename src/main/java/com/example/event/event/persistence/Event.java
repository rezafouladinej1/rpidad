package com.example.event.event.persistence;

import com.example.event.base.DatabaseConfig;
import com.example.event.category.persistence.Category;
import com.example.event.registration.persistence.Registration;
import com.example.event.user.persistence.User;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "events",schema = DatabaseConfig.DB)
public class Event {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Integer id;

    @Column(name = "status")
    private Integer status;

    @Column(name = "price")
    private Integer price;

    @Column(name = "capacity")
    private Integer capacity;

    @Column(name = "time")
    private String time;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id",referencedColumnName = "id")
    private Category category;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private User user;

    @OneToMany(mappedBy = "event",fetch = FetchType.LAZY)
    private List<Registration> events;

}
